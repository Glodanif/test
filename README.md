## **Test assignment for one startup company** ##
Completion time ~4h

## **Classes:** ##
com.g.test.ui.MainActivity - main container, runs two fragments and Tabs+VidwPager, dispatch events between fragments

com.g.test.ui.fragments.TagInputFragment - fragment for a tag input, wnen user clicks "Next" button fragments triggers observer OnTagSubmittedListener

com.g.test.ui.fragments.VideosFragment - container for videos, other classes can call public fetchVideos() method to load videos from the Internet

com.g.test.ui.utils.CircleTransform - Picasso transform for making rounded images

com.g.test.ui.utils.LazyLoadOnScrollListener - class that provides us with an event that end of list was reached to trigger pagination

com.g.test.ui.views.VideoContainer - container for VideoView that keeps aspect ratio 16:9

com.g.test.ui.adapters.PagerAdapter - adapter for ViewPager, initialize two fragments and provides getters, so we can get their instances

com.g.test.ui.adapters.VideosAdapter - adapter for video list, has methods to set and get currently playing videos to highlight them with blue color

com.g.test.service.ApiService - interface that represents source of data

com.g.test.service.VinoApiService - implementation

com.g.test.service.responses.TagSearchResponse - represents whole API response

com.g.test.service.responses.TagSearchData - represents "data" object

com.g.test.service.responses.VideoItem - represents "record" object


## **Libraries:** ##
Retrofit 2 - for making REST call, convenient API

ButterKnife - to avoid boilerplate

Picasso - to load and cache images