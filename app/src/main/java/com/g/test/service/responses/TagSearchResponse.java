package com.g.test.service.responses;

public class TagSearchResponse {

    private TagSearchData data;
    private boolean success;
    private String error;

    public TagSearchData getData() {
        return data;
    }

    public boolean isSuccessful() {
        return success;
    }

    public String getError() {
        return error;
    }
}
