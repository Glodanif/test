package com.g.test.service;

import com.g.test.service.responses.TagSearchResponse;

import retrofit2.Call;

public interface ApiService {
    Call<TagSearchResponse> searchByTag(String tag, int pageId);
}
