package com.g.test.service.responses;

public class VideoItem {

    private String username;
    private String thumbnailUrl;
    private String videoLowURL;

    public String getUsername() {
        return username;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getVideoUrl() {
        return videoLowURL;
    }
}
