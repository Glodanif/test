package com.g.test.service.responses;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TagSearchData {

    private int count;
    private int  nextPage;
    private int size;

    @SerializedName("records")
    private List<VideoItem> videoList;

    public int getCount() {
        return count;
    }

    public int getNextPage() {
        return nextPage;
    }

    public int getSize() {
        return size;
    }

    public List<VideoItem> getVideoList() {
        return videoList;
    }
}
