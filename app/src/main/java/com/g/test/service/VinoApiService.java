package com.g.test.service;

import com.g.test.service.responses.TagSearchResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class VinoApiService implements ApiService {

    private final VinoService gitHubService;

    public VinoApiService() {
        gitHubService = VinoService.RETROFIT.create(VinoService.class);
    }

    @Override
    public Call<TagSearchResponse> searchByTag(String tag, int pageId) {
        return gitHubService.repoContributors(tag, pageId);
    }

    public interface VinoService {

        Retrofit RETROFIT = new Retrofit.Builder()
                .baseUrl("https://api.vineapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        @GET("timelines/tags/{tag}")
        Call<TagSearchResponse> repoContributors(@Path("tag") String tag, @Query("page") int pageId);
    }
}
