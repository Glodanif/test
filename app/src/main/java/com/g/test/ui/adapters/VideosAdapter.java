package com.g.test.ui.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.g.test.R;
import com.g.test.service.responses.VideoItem;
import com.g.test.ui.utils.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 1;

    private List<VideoItem> videos = new ArrayList<>();

    private final Context context;

    private OnClickListener listener;

    private boolean showFooter;

    private VideoItem currentVideo;

    @ColorInt
    private final int textColor;
    @ColorInt
    private final int textColorSelected;

    public VideosAdapter(Context context) {
        this.context = context;
        this.textColor = context.getResources().getColor(R.color.username);
        this.textColorSelected = context.getResources().getColor(R.color.username_selected);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FOOTER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_footer, parent, false);
            return new FooterViewHolder(view);
        }

        View view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            footerHolder.itemView.setVisibility(showFooter ? View.VISIBLE : View.GONE);
            footerHolder.progress.setVisibility(showFooter ? View.VISIBLE : View.GONE);
            return;
        }

        ItemViewHolder itemHolder = (ItemViewHolder) holder;

        VideoItem item = videos.get(position);

        itemHolder.username.setText(item.getUsername());
        itemHolder.username.setTextColor(item.equals(currentVideo) ? textColorSelected : textColor);
        itemHolder.playingIndicator.getIndeterminateDrawable().setColorFilter(textColorSelected, PorterDuff.Mode.MULTIPLY);
        itemHolder.playingIndicator.setVisibility(item.equals(currentVideo) ? View.VISIBLE : View.GONE);
        Picasso.with(context)
                .load(item.getThumbnailUrl())
                .placeholder(R.drawable.circle)
                .error(R.drawable.circle)
                .transform(new CircleTransform())
                .into(itemHolder.thumbnail);

        itemHolder.itemView.setOnClickListener(v -> {

            currentVideo = item;
            itemHolder.username.setTextColor(textColorSelected);
            notifyDataSetChanged();

            if (listener != null) {
                listener.onClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videos.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionFooter(position)) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionFooter(int position) {
        return position == videos.size();
    }

    public void setFooterVisible(boolean showFooter) {
        this.showFooter = showFooter;
    }

    public void setCurrentItem(VideoItem item) {
        currentVideo = item;
        notifyDataSetChanged();
    }

    @Nullable
    public VideoItem getNextVideo() {

        if (currentVideo == null || videos.isEmpty()) {
            return null;
        }

        int currentPosition = videos.indexOf(currentVideo);
        return currentPosition + 1 >= videos.size() ? null : videos.get(++currentPosition);
    }

    public int getCurrentVideoPosition() {
        return currentVideo == null || videos.isEmpty() ? 0 : videos.indexOf(currentVideo);
    }

    public void addVideos(List<VideoItem> videos) {
        this.videos.addAll(videos);
    }

    public void clearVideos() {
        this.videos.clear();
    }

    public void setOnItemClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_thumbnail)
        ImageView thumbnail;
        @BindView(R.id.tv_username)
        TextView username;
        @BindView(R.id.pb_plying_indicator)
        ProgressBar playingIndicator;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pb_lazy_load_progress)
        public ProgressBar progress;

        public FooterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnClickListener {
        void onClick(VideoItem videoItem);
    }
}


