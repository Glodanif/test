package com.g.test.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.g.test.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnEditorAction;

public class TagInputFragment extends Fragment {

    @BindView(R.id.et_tag_field)
    EditText tagField;
    @BindView(R.id.til_tag_input)
    protected TextInputLayout tagInput;

    private OnTagSubmittedListener tagSubmittedListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tag_input, null);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @OnEditorAction(R.id.et_tag_field)
    protected boolean submit(int actionId) {

        if (actionId == EditorInfo.IME_ACTION_NEXT) {

            String text = tagField.getText().toString().trim();

            if (TextUtils.isEmpty(text)) {
                tagInput.setError(getString(R.string.emptyTextError));
            } else {
                tagInput.setError(null);
                if (tagSubmittedListener != null) {
                    tagSubmittedListener.onTagSubmitted(text);
                }
            }

            return true;
        }
        return false;
    }

    public void setOnTagSubmittedListener(OnTagSubmittedListener tagSubmittedListener) {
        this.tagSubmittedListener = tagSubmittedListener;
    }

    public interface OnTagSubmittedListener {
        void onTagSubmitted(String tag);
    }
}
