package com.g.test.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.g.test.R;
import com.g.test.ui.fragments.VideosFragment;
import com.g.test.ui.adapters.PagerAdapter;
import com.g.test.ui.fragments.TagInputFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tl_sliding_tabs)
    protected TabLayout tabLayout;
    @BindView(R.id.vp_lists)
    protected ViewPager container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        PagerAdapter adapter = new PagerAdapter(this, getSupportFragmentManager());
        TagInputFragment tagInputFragment = adapter.getTagInputFragment();
        VideosFragment videosFragment = adapter.getVideosFragment();

        tagInputFragment.setOnTagSubmittedListener(tag -> {
            hideKeyboard();
            container.setCurrentItem(1);
            videosFragment.fetchVideos(tag);
        });

        container.setAdapter(adapter);
        tabLayout.setupWithViewPager(container);
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null && inputManager != null) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            inputManager.showSoftInputFromInputMethod(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
