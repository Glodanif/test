package com.g.test.ui.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.g.test.R;
import com.g.test.service.ApiService;
import com.g.test.service.VinoApiService;
import com.g.test.service.responses.TagSearchData;
import com.g.test.service.responses.TagSearchResponse;
import com.g.test.service.responses.VideoItem;
import com.g.test.ui.adapters.VideosAdapter;
import com.g.test.ui.utils.LazyLoadOnScrollListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideosFragment extends Fragment {

    private static final int LAZY_LOAD_OFFSET = 3;

    @BindView(R.id.vv_video)
    VideoView videoView;
    @BindView(R.id.rv_video_list)
    RecyclerView videoList;
    @BindView(R.id.ll_loaded_videos)
    LinearLayout loadedVideosContainer;
    @BindView(R.id.pb_progress)
    ProgressBar progressBar;
    @BindView(R.id.tv_no_videos)
    TextView noVideosMessage;
    @BindView(R.id.pb_video_progress)
    ProgressBar videoLoadingProgress;

    private Activity activity;

    private VideosAdapter adapter;
    private LinearLayoutManager layoutManager;

    private ApiService apiService;

    private String tag;
    private int pageToLoad = 1;

    private final LazyLoadOnScrollListener scrollListener = new LazyLoadOnScrollListener(LAZY_LOAD_OFFSET) {
        @Override
        public void onLoad() {
            searchVideosByTag(tag, pageToLoad);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_videos, null);
        ButterKnife.bind(this, rootView);

        apiService = new VinoApiService();

        videoView.setOnPreparedListener(mp -> {
            videoView.start();
            videoLoadingProgress.setVisibility(View.GONE);
        });
        videoView.setOnCompletionListener(mp -> loadNextVideo());

        adapter = new VideosAdapter(activity);
        adapter.setOnItemClickListener(videoItem -> loadVideo(videoItem.getVideoUrl()));

        layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        videoList.setLayoutManager(layoutManager);
        videoList.setAdapter(adapter);
        videoList.setOnScrollListener(scrollListener);

        return rootView;
    }

    private void loadNextVideo() {
        VideoItem item = adapter.getNextVideo();
        if (item != null) {
            layoutManager.scrollToPositionWithOffset(adapter.getCurrentVideoPosition(), 0);
            adapter.setCurrentItem(item);
            loadVideo(item.getVideoUrl());
        }
    }

    private void loadVideo(String url) {
        clearCurrentFrame();
        videoView.setVideoURI(Uri.parse(url));
        videoLoadingProgress.setVisibility(View.VISIBLE);
    }

    private void clearCurrentFrame() {
        videoView.setVisibility(View.GONE);
        videoView.setVisibility(View.VISIBLE);
    }

    public void fetchVideos(String tag) {
        this.tag = tag;
        adapter.clearVideos();
        searchVideosByTag(tag, 1);
        progressBar.setVisibility(View.VISIBLE);
        loadedVideosContainer.setVisibility(View.GONE);
        noVideosMessage.setVisibility(View.GONE);
        videoView.stopPlayback();
    }

    private void searchVideosByTag(String tag, int pageId) {

        if (pageId > 1) {
            adapter.setFooterVisible(true);
        }

        apiService.searchByTag(tag, pageId).enqueue(new Callback<TagSearchResponse>() {

            @Override
            public void onResponse(Call<TagSearchResponse> call, Response<TagSearchResponse> response) {

                adapter.setFooterVisible(false);
                progressBar.setVisibility(View.GONE);

                if (response != null && response.isSuccessful() &&
                        response.body() != null && response.body().isSuccessful()) {

                    TagSearchData data = response.body().getData();
                    if (data.getVideoList() != null && !data.getVideoList().isEmpty()) {
                        loadedVideosContainer.setVisibility(View.VISIBLE);
                        adapter.addVideos(data.getVideoList());
                        adapter.notifyDataSetChanged();
                        pageToLoad = data.getNextPage();
                        scrollListener.notifyFinish();
                    } else {
                        noVideosMessage.setVisibility(View.VISIBLE);
                    }

                    if (data.getNextPage() == 0) {
                        scrollListener.setEnabled(false);
                    }

                } else {
                    scrollListener.notifyFail();
                    showFailPopup();
                }
            }

            @Override
            public void onFailure(Call<TagSearchResponse> call, Throwable t) {
                adapter.setFooterVisible(false);
                progressBar.setVisibility(View.GONE);
                scrollListener.notifyFail();
                showFailPopup();
            }
        });
    }

    private void showFailPopup() {

        if (isAdded()) {
            Snackbar.make(videoList, R.string.unableToLoad, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.tryAgain, view -> {
                        if (pageToLoad == 1) {
                            progressBar.setVisibility(View.VISIBLE);
                        }
                        searchVideosByTag(tag, pageToLoad);
                    })
                    .show();
        }
    }
}
