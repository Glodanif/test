package com.g.test.ui.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public abstract class LazyLoadOnScrollListener extends RecyclerView.OnScrollListener {

    private int startLoadOffset;

    private boolean isLoading, isLastCallFailed;

    private LinearLayoutManager layoutManager;

    private boolean isEnabled = true;

    public LazyLoadOnScrollListener(int startLoadOffset) {
        this.startLoadOffset = startLoadOffset;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (layoutManager == null) {
            if (!(recyclerView.getLayoutManager() instanceof LinearLayoutManager)) {
                throw new IllegalArgumentException("RecyclerViewLazyLoadOnScrollListener supports only LinearLayoutManager");
            }
            layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        }

        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
        int visibleItemCount = layoutManager.findLastVisibleItemPosition() - layoutManager.findFirstVisibleItemPosition();

        if (!isEnabled) {
            return;
        }

        boolean offsetReached = totalItemCount - (firstVisibleItem + visibleItemCount) <= startLoadOffset;

        if (totalItemCount > 0 && offsetReached && !isLoading && !isLastCallFailed) {
            isLoading = true;
            onLoad();
        }
    }

    public abstract void onLoad();

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public void notifyFail() {
        isLoading = false;
        isLastCallFailed = true;
    }

    public void notifyFinish() {
        isLoading = isLastCallFailed = false;
    }

    public void reset() {
        isLoading = isLastCallFailed = false;
    }
}
