package com.g.test.ui.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.g.test.R;
import com.g.test.ui.fragments.TagInputFragment;
import com.g.test.ui.fragments.VideosFragment;

/**
 * Created by Vasyl on 24.11.2016.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    private final Context context;

    private final TagInputFragment tagInputFragment;
    private final VideosFragment videosFragment;

    public PagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        this.tagInputFragment = new TagInputFragment();
        this.videosFragment = new VideosFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(position == 0 ? R.string.tag_tab : R.string.video_tab);
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0 ? tagInputFragment : videosFragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public TagInputFragment getTagInputFragment() {
        return tagInputFragment;
    }

    public VideosFragment getVideosFragment() {
        return videosFragment;
    }
}
